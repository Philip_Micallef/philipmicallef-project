﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class scenemanager : MonoBehaviour {

	//Creating Buttons
	Button PlayButton, HowTo, Back;



	// Use this for initialization
	void Start () {

        //Initializing the current scene
		Scene currentScene = SceneManager.GetActiveScene();

        //Storing current scene
		string sceneName = currentScene.name;

        
		if (sceneName == "mainMenu")
		{
         
			//Assigning Buttons to game objects & creating an event listener 
			PlayButton = GameObject.Find("PlayButton").GetComponent<Button>();
			PlayButton.onClick.AddListener(playGame);

			HowTo = GameObject.Find("HowToPlayButton").GetComponent<Button>();
			HowTo.onClick.AddListener(instructions);
         
		}

		if (sceneName == "instructions")
        {    
            Back = GameObject.Find("BackButton").GetComponent<Button>();
            Back.onClick.AddListener(mainMenu);
        }      
	}
    

	void playGame()
	{
		SceneManager.LoadScene ("game");
	}

	void instructions()
	{
		SceneManager.LoadScene("instructions");
	}

	void mainMenu()
	{
		SceneManager.LoadScene("mainMenu");
	}

   
	void OnTriggerEnter2D(Collider2D player)
    {
        if(player.gameObject.name == "Player"){
            SceneManager.LoadScene("Win");        
        }
    }
        
	// Update is called once per frame
	void Update () {
		
	}
}
