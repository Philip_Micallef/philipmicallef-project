﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class gateCollision : MonoBehaviour
{

	GameObject player;
	GameObject gate1;
	GameObject gate2;
	GameObject gate3;
	GameObject gate4;
	GameObject gate5;
	GameObject gate6;
	GameObject gate7;
	GameObject gate8;

	public AudioSource teleportSound;

	void Start()
	{
		player = GameObject.Find("Player");
		gate1 = GameObject.Find("GateSize1");
		gate2 = GameObject.Find("GateSize2");
		gate3 = GameObject.Find("GateSize3");
		gate4 = GameObject.Find("GateSize4");
		gate5 = GameObject.Find("GateSize5");
		gate6 = GameObject.Find("GateSize6");
		gate7 = GameObject.Find("GateSize7");
		gate8 = GameObject.Find("GateSize8");

		teleportSound = GetComponent<AudioSource>();

		//Set inital colour to blue
		player.GetComponent<SpriteRenderer>().color = Color.blue;
	}



	void OnCollisionEnter2D(Collision2D collisionEvent)
	{
		//Comparing Player's Current Color with the next Gate. 
		//If they arent the same, player will be transported back to start
		//If they are, the player continues through the next gate

		if (collisionEvent.collider.gameObject.tag == "Player")
		{
			if (collisionEvent.gameObject.GetComponent<SpriteRenderer>().color != this.gameObject.GetComponent<SpriteRenderer>().color)
			{
				//Transport player back to start
				player.transform.position = new Vector3(5.0f, 1.0f, 0.0f);
				teleportSound.Play();

			}
			else
			{

				Debug.Log("I passed the gate");
				this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
			}
		}
	}

	void OnCollisionExit2D(Collision2D collisionEvent)
	{
		//Turn Gate White upon exiting collision
		GetComponent<SpriteRenderer>().color = Color.magenta;
	}
}
