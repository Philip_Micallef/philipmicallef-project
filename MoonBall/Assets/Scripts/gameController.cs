﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameController : MonoBehaviour {


	GameObject player;
	GameObject gate1;
	GameObject gate2;
	GameObject gate3;
	GameObject gate4;
	GameObject gate5;
    GameObject gate6;
    GameObject gate7;
    GameObject gate8;
	GameObject wall;

    //Array to store all insances of Tagged gameObjects
	GameObject[] diamond; 

	// Use this for initialization
	void Start () {

		player = GameObject.Find("Player");
		gate1 = GameObject.Find("GateSize1");
		gate2 = GameObject.Find("GateSize2");
		gate3 = GameObject.Find("GateSize3");
		gate4 = GameObject.Find("GateSize4");
		gate5 = GameObject.Find("GateSize5");
		gate6 = GameObject.Find("GateSize6");
		gate7 = GameObject.Find("GateSize7");
		gate8 = GameObject.Find("GateSize8");
		wall = GameObject.Find("DeleteBoarder");

	}
	
	// Update is called once per frame
	void Update () {



        //MOVING GATES and Setting Colour
		gate1.transform.Translate
            (Vector3.left * Time.deltaTime * 2);
		gate1.GetComponent<SpriteRenderer>().color = Color.red;
        
		gate2.transform.Translate
		     (Vector3.left * Time.deltaTime * 2);
		gate2.GetComponent<SpriteRenderer>().color = Color.blue;

		gate3.transform.Translate
		     (Vector3.left * Time.deltaTime * 2);
		gate3.GetComponent<SpriteRenderer>().color = Color.yellow;

		gate4.transform.Translate
		     (Vector3.left * Time.deltaTime * 2);
		gate4.GetComponent<SpriteRenderer>().color = Color.green;

		gate5.transform.Translate
            (Vector3.left * Time.deltaTime * 2);
        gate5.GetComponent<SpriteRenderer>().color = Color.red;

		gate6.transform.Translate
             (Vector3.left * Time.deltaTime * 2);
		gate6.GetComponent<SpriteRenderer>().color = Color.blue;

		gate7.transform.Translate
             (Vector3.left * Time.deltaTime * 2);
		gate7.GetComponent<SpriteRenderer>().color = Color.yellow;

		gate8.transform.Translate
             (Vector3.left * Time.deltaTime * 2);
		gate8.GetComponent<SpriteRenderer>().color = Color.green;


        //Diamonds movement
		diamond = GameObject.FindGameObjectsWithTag("Diamond");
		foreach (GameObject d in diamond)
			d.transform.Translate(Vector3.left * Time.deltaTime * 1.5f);
       

	}

	private void OnCollisionEnter2D(Collision2D collision)
	{

		if (collision.gameObject.name == "DeleteBoarder")
        {
			Destroy(collision.gameObject);
        }
	}
       

}
