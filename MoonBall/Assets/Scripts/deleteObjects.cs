﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deleteObjects : MonoBehaviour {



    //This is not going to work if other objects are triggers

    //Objects get deleted when out of screen
    /*
	void OnCollisionEnter2D(Collision2D collisionEvent)
    {
		if (collisionEvent.gameObject.tag == "Gate")
		{
			Destroy(gameObject);
			Debug.Log("gate gone!");
		}
    }
    */

	void OnTriggerEnter2D(Collider2D otherObject)
    {
		if (otherObject.gameObject.tag == "Gate")
        {
            Destroy(gameObject);
            Debug.Log("gate gone!");
        }
    }
}
