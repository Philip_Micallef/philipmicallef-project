﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class playerController : MonoBehaviour
{
	//Initialise - rb allows direct access to its options 
	GameObject player;
	GameObject floor;
	Rigidbody2D rb;



	public float jumpSpeed;
	bool isGrounded = false;
	private int score;
	public Text scoreText;

	public AudioSource diamondSound;



	// Use this for initialization
	void Start()
	{

		diamondSound = GetComponent<AudioSource>();

		player = GameObject.Find("Player");

		floor = GameObject.Find("Floor");
		rb = GetComponent<Rigidbody2D>();

		score = 0;
		SetScoreText();
	}

	//Player contact with Diamond
	void OnTriggerEnter2D(Collider2D item)
	{
		if (item.gameObject.tag == "Diamond")
		{
			score++;
			Debug.Log("Diamond Collected");//+score);
			SetScoreText();

            //Play sound for collecting Diamond
			diamondSound.Play();

            //Destroy Diamond 
			Destroy(item.gameObject);
		}
	}

	void SetScoreText()
	{
		scoreText.text = "Score:" + score.ToString();
	}

	/*--------------------------------------------------------------
	//DOESNT WORK !!!!

	//Floor Collision Function 
	void OnCollisionEnter(Collision theCollision)
	{
		if (theCollision.gameObject.name == "Floor")
		{
			isGrounded = true;
			Debug.Log("Im on the floor");
		}
	}

	void OnCollisionExit(Collision theCollision)
	{
		if (theCollision.gameObject.name == "Floor")
		{
			isGrounded = false;

		}
	}
	--------------------------------------------------------------*/









	// Update is called once per frame
	void Update()
	{

		//Horizontal
		player.transform.Translate
		(Vector3.right * Input.GetAxis("Horizontal") * Time.deltaTime * 10);


		//LEFT 
		if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			player.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector3(-1f, 0f));
		}

		//RIGHT
		if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			player.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector3(1f, 0f));
		}



		//Check to see if player is on ground 
		//If true
		//if (isGrounded == true)
		//{
		//Jump
		if (Input.GetKeyDown(KeyCode.Space))
		{
			rb.velocity = new Vector3(rb.velocity.x, jumpSpeed);
			isGrounded = false;

		}
		//}

		//Sizing controls

		//Small
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			player.transform.localScale = new Vector3(-0.5f, -0.5f, 0);
		}

		//Big
		if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			player.transform.localScale = new Vector3(1f, 1f, 0);
		}

		//Bigger
		if (Input.GetKeyDown(KeyCode.Alpha3))
		{
			player.transform.localScale = new Vector3(3f, 3f, 0);
		}

		//Huge
		if (Input.GetKeyDown(KeyCode.Alpha4))
		{
			player.transform.localScale = new Vector3(5f, 5f, 0);
		}


		//Color Controls
		//--------------
		//Blue
		if (Input.GetKeyDown(KeyCode.B))
		{
			player.GetComponent<SpriteRenderer>().color = Color.blue;
		}

		//Green
		if (Input.GetKeyDown(KeyCode.G))
		{
			player.GetComponent<SpriteRenderer>().color = Color.green;
		}

		//Yellow
		if (Input.GetKeyDown(KeyCode.Y))
		{
			player.GetComponent<SpriteRenderer>().color = Color.yellow;
		}

		//Red
		if (Input.GetKeyDown(KeyCode.R))
		{
			player.GetComponent<SpriteRenderer>().color = Color.red;
		}
        
	}
}
//}